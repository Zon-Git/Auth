Vue.component('coolBtn', {
    props: ['name', 'type'],
    template: "<input @click='defaultClick'  :value='name' :class='type' type='button'\> ",
    methods: {
        defaultClick: function () {
            this.$emit('btn-click');
        }
    },
    created: function () {
        if (!this.type) {
            this.type = 'primary';
        }
    }

});