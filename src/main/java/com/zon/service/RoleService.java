package com.zon.service;

import java.util.List;

import com.zon.entity.Role;
import com.zon.payload.CreateRoleDto;
import com.zon.payload.UpdateRoleDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface RoleService {

  public Page<Role> getAllRoles(Pageable pageable);

  public Page<Role> getRolesWithPermissions(Pageable pageable);

  public Role addRole(CreateRoleDto roleDto);

  public Role updateRole(UpdateRoleDto roleDto, Long id);

  public Role getRoleByIdWithPermissions(Long id);

  public Boolean canDelete(List<Long> ids);

  public void deleteRoles(List<Long> ids);
}
