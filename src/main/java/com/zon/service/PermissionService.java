package com.zon.service;

import java.util.List;

import com.zon.entity.Permission;
import com.zon.payload.UpdatePermissionDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface PermissionService {

  public Page<Permission> getAllPermissions(Pageable pageable);

  public Permission updatePermission(UpdatePermissionDto roleDto, Long id);

  public Boolean canDelete(List<Long> ids);

  public void deletePermissions(List<Long> ids);
}
