package com.zon.service;

import java.util.List;

import com.zon.entity.User;
import com.zon.payload.CreateUserDto;
import com.zon.payload.CurrentResult;
import com.zon.payload.UpdateUserDto;
import com.zon.payload.UpdateUserSelfDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface UserService {
  public Page<User> getAllUsers(Pageable pageable);

  public Page<User> getAllUsersWithRoles(Pageable pageable);

  public User getUserById(Long id);

  public User getUserByIdWithRoles(Long id);

  public User addUser(CreateUserDto userDto);

  public void updateUser(UpdateUserDto userDto, Long id);

  public void updateUserBase(UpdateUserSelfDto userDto, Long id);

  public Boolean existsByUsername(String username);

  public void updateUser(User user);

  public void updateUserPassword(Long id, String password);

  public CurrentResult getCurrentUser(String username);

  public void deleteUsers(List<Long> ids);

  public void initAllUsers();

  public void clearAllUsers();
}


