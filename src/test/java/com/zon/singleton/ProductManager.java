package com.zon.singleton;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.ArrayList;
import java.util.List;

//双重检测
public class ProductManager {
    private static volatile ProductManager instance;
    private List<Product> products;

    private ProductManager() {
        products = new ArrayList<>();
    }
    public static ProductManager getInstance(  ){
        if (instance == null){
            synchronized (ProductManager.class) {
                if (instance == null){
                    instance = new ProductManager();
                }
            }
        }
        return instance;
    }
    public void addProduct(Product product){
        products.add(product);
    }

    public void removeProduct(Product product){
        products.remove(product);
    }
    public  void displayProducts(){
        System.out.println("Products list:");
        for (Product product : products) {
            System.out.println(product.getId()+"-"+product.getName());
        }
    }
}
