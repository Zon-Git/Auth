package com.zon.mediator;

import java.util.HashMap;
import java.util.Map;

/**
 *  聊天中介器
 */
public class ChatMediator {
    private Map<String,User> userMap = new HashMap<String, User>();

    public void registerUser(User user){
        userMap.put(user.getName(),user);
    }

    public void relayMessage(User sender,String receiverName,String message){
        User receiver = userMap.get(receiverName);
        if (receiver != null){
            System.out.println(sender.getName() + " sends a message to " + receiver.getName() + ": " + message);
            // 这里可以添加额外的逻辑，比如记录消息、通知其他服务等
        }else {
            System.out.println("Receiver " + receiverName + " is not registered.");
        }
    }
}
