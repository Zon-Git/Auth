package com.zon.mediator;

public class MediatorClient {
    public static void main(String[] args) {
        // 创建中介者 用户
        ChatMediator chatMediator = new ChatMediator();
        User alice = new User("Alice",chatMediator);
        User bob  = new User("Bob",chatMediator);

        // 注册用户
        chatMediator.registerUser(alice);
        chatMediator.registerUser(bob);

        // Alice通过中介者发送消息给Bob
        alice.sendMessage("Bob", "Hi Bob, how are you?");

        // Bob通过中介者回复消息给Alice
        bob.sendMessage("Alice", "I'm good, thanks!");

        // 尝试发送消息给一个未注册的用户
        alice.sendMessage("Charlie", "Hey Charlie, are you there?");

    }
}
