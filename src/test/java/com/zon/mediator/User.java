package com.zon.mediator;

/**
 *
 */
public class User {
    private String name;
    private ChatMediator chatMediator;

    public User(String name, ChatMediator chatMediator) {
        this.name = name;
        this.chatMediator = chatMediator;
    }

    public String getName() {
        return name;
    }


    // 用户通过中介者发送消息
    public void sendMessage(String receiverName, String message) {
        chatMediator.relayMessage(this, receiverName, message);
    }
}
