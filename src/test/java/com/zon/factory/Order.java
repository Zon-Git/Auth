package com.zon.factory;

public interface Order {
    void process();
    // 其他订单操作方法
}
