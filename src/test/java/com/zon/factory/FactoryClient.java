package com.zon.factory;

public class FactoryClient {
    public static void main(String[] args) {
        OrderFactory factory = new OrderFactory();
        OrderProcessor processor = new OrderProcessor(factory);
        // 正常订单
        processor.process("Normal");
        //团购订单
        processor.process("Group");
        // 优惠订单
        processor.process("Discount");
    }
}
