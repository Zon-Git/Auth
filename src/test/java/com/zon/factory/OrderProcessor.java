package com.zon.factory;

public class OrderProcessor {
    private OrderFactory orderFactory;

    public OrderProcessor(OrderFactory orderFactory) {
        this.orderFactory = orderFactory;
    }

    public void process(String orderType) {
        Order order = orderFactory.createOrder(orderType);
        order.process();
    }
}
