package com.zon.factory;

public class DiscountOrder  implements  Order{
    @Override
    public void process() {
        System.out.println("Process Discount Order");
    }
}
