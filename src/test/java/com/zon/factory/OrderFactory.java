package com.zon.factory;

public class OrderFactory {
    public Order createOrder(String orderType) {
        if (orderType.equals("Normal")){
            return new NormalOrder();
        }else if (orderType.equals("Group")){
            return new GroupOrder();
        }else if (orderType.equals("Discount")){
            return new DiscountOrder();
        }else {
            throw new IllegalArgumentException("Invalid order type: " + orderType);
        }
    }
}
