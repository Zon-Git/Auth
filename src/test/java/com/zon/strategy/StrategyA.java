package com.zon.strategy;

// 定义具体策略角色（Concrete Strategy）：每个节日具体的促销活动
//为春节准备的促销活动A
public class StrategyA implements Strategy{

    @Override
    public void show() {
        System.out.println("买一送一");
    }
}
