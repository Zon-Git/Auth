package com.zon.strategy;

import java.util.HashMap;
import java.util.Map;

// 定义环境角色（Context）：用于连接上下文，即把促销活动推销给客户，这里可以理解为销售员
public class SalesStrategy {
    public static final String SaleKeyA = "A";
    public static final String SaleKeyB = "B";
    public static final String SaleKeyC = "C";

    private static Map<String, Strategy> sales = new HashMap<>();

    static {
        sales.put(SaleKeyA, new StrategyA());
        sales.put(SaleKeyB, new StrategyB());
        sales.put(SaleKeyC, new StrategyC());
    }

    public Strategy getStarategy(String key) {
       Strategy strategy = sales.get(key);
       if (strategy == null){
           throw new RuntimeException("没有找到对应的策略");
       }
       return strategy;
    }
}
