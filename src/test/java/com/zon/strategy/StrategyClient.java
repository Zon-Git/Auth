package com.zon.strategy;

//客户端
public class StrategyClient {
    public static void main(String[] args) {
        // 春节来了 使用春节促销活动
        SalesStrategy salesMan = new SalesStrategy();
        String saleKey = "D";

        Strategy strategy = salesMan.getStarategy(saleKey);
        // 展示促销活动
        strategy.show();

    }
}
