package com.zon.strategy.order;

public class Client {
    public static void main(String[] args) {
        Order order = new Order("1001", "20190101001", 300.0);
        System.out.println(order.pay(PayStrategy.UNION_PAY));
    }
}
