package com.zon.strategy.order;

import java.util.HashMap;
import java.util.Map;

// 支付策略管理类
public class PayStrategy {
    public static final String ALI_PAY = "AliPay";
    public static final String JD_PAY = "JDPay";
    public static final String WECHAT_PAY = "WebChatPay";
    public static final String UNION_PAY = "UnionPay";
    public static final String DEFAULT_PAY = ALI_PAY;

    private static Map<String, Payment> strategyMap = new HashMap<>();
    static {
        strategyMap.put(ALI_PAY, new AliPay());
        strategyMap.put(JD_PAY, new JDPay());
        strategyMap.put(WECHAT_PAY, new WebChatPay());
        strategyMap.put(UNION_PAY, new UnionPay());
    }

    public static Payment getPay(String payKey){
        if(!strategyMap.containsKey(payKey)){
            return strategyMap.get(DEFAULT_PAY);
        }
        return strategyMap.get(payKey);
    }
}
