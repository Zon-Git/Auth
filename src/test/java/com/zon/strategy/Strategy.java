package com.zon.strategy;
// 定义百货公司所有促销活动的共同接口
public interface Strategy {
    void show();
}
