package com.zon.observer;
//观察者接口
public interface Observer {
    /**
     * 更新操作
     * @param state 状态
     */
    void update(String state);
}
