package com.zon.observer;

import com.zon.observer.Observer;

//具体的观察者类
public class ConcreteObserver implements Observer {

    private String  observerState;

    @Override
    public void update(String state) {
        this.observerState = state;
        System.out.println("ConcreteObserver's state updated to: " + state);
    }

    public String getObserverState() {
        return observerState;
    }
}
