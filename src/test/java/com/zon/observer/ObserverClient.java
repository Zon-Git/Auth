package com.zon.observer;

public class ObserverClient {
    public static void main(String[] args) {
        // 创建具体主题和观察者
        ConcreteSubject subject = new ConcreteSubject();
        Observer observer = new ConcreteObserver();

        // 注册观察者
        subject.registerObserver(observer);
        // 改变主题状态
        subject.setState("new state");

        // 取消注册观察者
        subject.removeObserver(observer);

        // 再次改变主题状态，此时观察者不再接收通知
        subject.setState("another new state");
    }
}
