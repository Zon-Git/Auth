package com.zon.chain;

import lombok.Data;

/**
 *  实现统计处理
 */
@Data
public class StatisticsCollector  implements PaymentCallbackHandler{
    private  PaymentCallbackHandler nextHandler;
    @Override
    public void setNext(PaymentCallbackHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public void handle(PaymentCallbackContext context) {
        if (context.isNotificationSent()){
            System.out.println("统计订单支付成功数量和金额...");
            // 统计逻辑
        }else {
            System.out.println("未发送通知,不进行统计...");
        }
    }
}
