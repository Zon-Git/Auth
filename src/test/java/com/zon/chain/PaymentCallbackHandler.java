package com.zon.chain;

// 支付回调处理
public interface PaymentCallbackHandler {
    // 设置下一个责任链
    void setNext(PaymentCallbackHandler handler);
    // 方法处理
    void handle(PaymentCallbackContext context);
}
