package com.zon.chain;

public class ChainClient {
    public static void main(String[] args) {
        startPaymentCallbackChain();
    }

    private static void startPaymentCallbackChain() {
        //创建处理器实例
        PaymentCallbackHandler signatureValidator = new SignatureValidator();
        PaymentCallbackHandler orderStatusUpdater = new OrderStatusUpdater();
        PaymentCallbackHandler notificationSender = new NotificationSender();
        PaymentCallbackHandler statisticsCollector = new StatisticsCollector();

        //构建责任链
        signatureValidator.setNext(orderStatusUpdater);
        orderStatusUpdater.setNext(notificationSender);
        notificationSender.setNext(statisticsCollector);

        //执行责任链
        signatureValidator.handle(new PaymentCallbackContext());
    }
}
