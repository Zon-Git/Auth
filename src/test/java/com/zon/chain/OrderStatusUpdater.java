package com.zon.chain;

import lombok.Data;

@Data
public class OrderStatusUpdater  implements  PaymentCallbackHandler{
    private PaymentCallbackHandler nextHandler;
    @Override
    public void setNext(PaymentCallbackHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public void handle(PaymentCallbackContext context) {
            if (context.isSignatureValid()){
                System.out.println("更新订单状态...");
                // 更新订单状态
                context.setOrderStatusUpdated(true);
                // 继续执行下一个处理
                if (nextHandler != null){
                    nextHandler.handle(context);
                }
            }else {
                System.out.println("签名验证失败，不更新订单状态...");
            }
    }
}
