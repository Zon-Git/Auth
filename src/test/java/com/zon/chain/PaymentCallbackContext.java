package com.zon.chain;

import lombok.Data;

// 支付回调上下文类
@Data
class PaymentCallbackContext {
    // 签名是否有效
    private boolean signatureValid;
    // 订单状态是否更新
    private boolean orderStatusUpdated;
    // 通知是否发送
    private boolean notificationSent;

    public boolean isSignatureValid() {
        return signatureValid;
    }

    public boolean isOrderStatusUpdated() {
        return orderStatusUpdated;
    }

    public boolean isNotificationSent() {
        return notificationSent;
    }

}
