package com.zon.chain;


//验签处理

public class SignatureValidator  implements PaymentCallbackHandler{
    private  PaymentCallbackHandler nextHandler;
    @Override
    public void setNext(PaymentCallbackHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public void handle(PaymentCallbackContext context) {
         System.out.println("验证签名...");
         //验证逻辑
         context.setSignatureValid(true);
         //传递给下一个处理器
         nextHandler.handle(context);
    }
}
