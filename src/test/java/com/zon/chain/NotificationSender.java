package com.zon.chain;


import lombok.Data;

/**
 * 发送通知处理器
 */
@Data
public class NotificationSender  implements PaymentCallbackHandler{
    private  PaymentCallbackHandler nextHandler;
    @Override
    public void setNext(PaymentCallbackHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public void handle(PaymentCallbackContext context) {
            if (context.isOrderStatusUpdated()){
                System.out.println("发送支付成功通知...");
                // 发送通知逻辑
                context.setNotificationSent(true);
                // 继续执行下一个处理器
                if (nextHandler != null){
                    nextHandler.handle(context);
                }
            }else {
                System.out.println("订单状态未更新，不发送通知...");
            }
    }
}
